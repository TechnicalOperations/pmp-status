import pandas
import locale
import MySQLdb
import numpy as np
from util import sql
from report_mailer import mailer
from datetime import datetime, timedelta
pandas.set_option('display.width', 5000)

BASE_AUTH_DIR = '/home/cbeecher/.auth/'
# BASE_AUTH_DIR = 'D:\\Work\\auth\\'
m = mailer.Mailer(BASE_AUTH_DIR + '.em_json', 'template.html')
con = MySQLdb.connect(read_default_file=BASE_AUTH_DIR + '.my.conf', db='dashboard')
date = datetime.now()
date = date - timedelta(hours=date.hour, days=4)
q = """
	SELECT *
	FROM index_pmpperformance
	WHERE date >= STR_TO_DATE('{start}', '%Y-%m-%d')
""".format(start=date.strftime('%Y-%m-%d'))
data = pandas.read_sql_query(q, con)
con.close()
data = data.groupby(['date', 'deal_id', 'ssp_id', 'dsp_id']).sum().reset_index()

radium_ids = ['1227', '1226', '1225', '1224', '1224', '1224']
q = """
SELECT A.external_deal_id AS deal_id,
    A.description,
    CASE WHEN A.type = 1 THEN 'legacy_direct'
        WHEN A.type = 2 THEN 'always_on'
        WHEN A.type = 3 THEN 'managed'
        WHEN A.type = 4 THEN 'self_serve'
        WHEN A.type = 5 THEN 'test'
    ELSE 'none_selected' END AS deal_type,
    IFF(B.dsp_id IN ({0}), 1, 0) AS is_radium
FROM ods.rx.deal AS A
LEFT JOIN ods.rx.deal_dsp AS B ON A.deal_id=B.deal_id
GROUP BY A.external_deal_id, A.description, deal_type, is_radium
""".format(", ".join(radium_ids))
con = sql.connect()
details = pandas.read_sql_query(q, con)
sql.close()
details = details.rename(columns={i: i.lower() for i in details.columns})

try:
    locale.setlocale(locale.LC_NUMERIC, 'en_US')
except locale.Error:
    locale.setlocale(locale.LC_NUMERIC, 'american english')


odata = data.copy()
odetails = details.copy()


#radium managed
data = odata[odata['deal_id'].isin(set(odetails[(odetails['deal_type'] == 'managed') & (odetails['is_radium'] == 1)]))]
details = details.to_dict(orient='records')
details = {i['deal_id']: i['description'] for i in details}

deals = data[(data['date'] == data['date'].max())][['deal_id', 'margin', 'revenue']].groupby('deal_id').sum().reset_index()
deals = deals.sort_values(by='revenue', ascending=False).reset_index(drop=True)
top = set(deals['deal_id'][:6])
deals['deal_id'] = deals['deal_id'].apply(lambda x: details.get(x, 'Managed - No Deal') if x in top else 'Other')
deals = deals.groupby('deal_id').sum().sort_values(by='revenue', ascending=False).reset_index()
top_margin = deals[['deal_id', 'margin']]
top_revenue = deals[['deal_id', 'revenue']]
total = data[['date', 'revenue', 'margin']].groupby('date').sum().reset_index()

m.make_graph('radium_managed_top_revenue', top_revenue, title='Revenue')
legend = m.make_graph('radium_managed_top_margin', top_margin, title='Margin')
m.make_graph('radium_managed_total', total, colors=['#16d1f0', '#fedd00'], title=None)

legend = list(legend)
legend.sort(key=lambda x: 0 - top_revenue[(top_revenue['deal_id'] == x[0])]['revenue'].sum() if x[0] != 'Other' else 0)
legend = [(i[0], i[1], m.format_float(top_revenue[(top_revenue['deal_id'] == i[0])]['revenue'].sum()),) for i in legend]
radium_managed_top_legend = "\n".join([
    """<tr><td style="background-color: {1}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>{0}</td><td>${2}</td></tr>""".format(i[0], i[1], i[2])
    for i in legend
])

radium_managed_total_revenue = int(np.round(data[(data['date'] == data['date'].max())]['revenue'].sum(), 0))
radium_managed_total_margin = int(np.round(data[(data['date'] == data['date'].max())]['margin'].sum(), 0))

#self service
data = odata[odata['deal_id'].isin(set(odetails[(odetails['deal_type'] == 'self_serve')]['deal_id']))]

deals = data[(data['date'] == data['date'].max())][['deal_id', 'margin', 'revenue']].groupby('deal_id').sum().reset_index()
deals = deals.sort_values(by='revenue', ascending=False).reset_index(drop=True)
top = set(deals['deal_id'][:6])
deals['deal_id'] = deals['deal_id'].apply(lambda x: details.get(x, 'Managed - No Deal') if x in top else 'Other')
deals = deals.groupby('deal_id').sum().sort_values(by='revenue', ascending=False).reset_index()
top_margin = deals[['deal_id', 'margin']]
top_revenue = deals[['deal_id', 'revenue']]
total = data[['date', 'revenue', 'margin']].groupby('date').sum().reset_index()

m.make_graph('self_serve_top_revenue', top_revenue, title='Revenue')
legend = m.make_graph('self_serve_top_margin', top_margin, title='Margin')
m.make_graph('self_serve_total', total, colors=['#16d1f0', '#fedd00'], title=None)

legend = list(legend)
legend.sort(key=lambda x: 0 - top_revenue[(top_revenue['deal_id'] == x[0])]['revenue'].sum() if x[0] != 'Other' else 0)
legend = [(i[0], i[1], m.format_float(top_revenue[(top_revenue['deal_id'] == i[0])]['revenue'].sum()),) for i in legend]
self_serve_top_legend = "\n".join([
    """<tr><td style="background-color: {1}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>{0}</td><td>${2}</td></tr>""".format(i[0], i[1], i[2])
    for i in legend
])

self_serve_total_revenue = int(np.round(data[(data['date'] == data['date'].max())]['revenue'].sum(), 0))
self_serve_total_margin = int(np.round(data[(data['date'] == data['date'].max())]['margin'].sum(), 0))

#always on
data = odata[odata['deal_id'].isin(set(odetails[(odetails['deal_type'] == 'always_on')]['deal_id']))]

deals = data[(data['date'] == data['date'].max())][['deal_id', 'margin', 'revenue']].groupby('deal_id').sum().reset_index()
deals = deals.sort_values(by='revenue', ascending=False).reset_index(drop=True)
top = set(deals['deal_id'][:6])
deals['deal_id'] = deals['deal_id'].apply(lambda x: details.get(x, 'Managed - No Deal') if x in top else 'Other')
deals = deals.groupby('deal_id').sum().sort_values(by='revenue', ascending=False).reset_index()
top_margin = deals[['deal_id', 'margin']]
top_revenue = deals[['deal_id', 'revenue']]
total = data[['date', 'revenue', 'margin']].groupby('date').sum().reset_index()

m.make_graph('ao_top_revenue', top_revenue, title='Revenue')
legend = m.make_graph('ao_top_margin', top_margin, title='Margin')
m.make_graph('ao_total', total, colors=['#16d1f0', '#fedd00'], title=None)

legend = list(legend)
legend.sort(key=lambda x: 0 - top_revenue[(top_revenue['deal_id'] == x[0])]['revenue'].sum() if x[0] != 'Other' else 0)
legend = [(i[0], i[1], m.format_float(top_revenue[(top_revenue['deal_id'] == i[0])]['revenue'].sum()),) for i in legend]
ao_top_legend = "\n".join([
    """<tr><td style="background-color: {1}">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td><td>{0}</td><td>${2}</td></tr>""".format(i[0], i[1], i[2])
    for i in legend
])

ao_total_revenue = int(np.round(data[(data['date'] == data['date'].max())]['revenue'].sum(), 0))
ao_total_margin = int(np.round(data[(data['date'] == data['date'].max())]['margin'].sum(), 0))

current_date = odata['date'].max().strftime('%m/%d/%Y')
total_revenue = int(np.round(odata[(odata['date'] == odata['date'].max())]['revenue'].sum(), 0))
total_margin = int(np.round(odata[(odata['date'] == odata['date'].max())]['margin'].sum(), 0))
m.set_formatter(formatter={
    'radium_managed_total_revenue': locale.format("%d", radium_managed_total_revenue, grouping=True),
    'radium_managed_total_margin': locale.format("%d", radium_managed_total_margin, grouping=True),
    'radium_managed_top_legend': radium_managed_top_legend,
    'self_serve_total_revenue': locale.format("%d", self_serve_total_revenue, grouping=True),
    'self_serve_total_margin': locale.format("%d", self_serve_total_margin, grouping=True),
    'self_serve_top_legend': self_serve_top_legend,
    'ao_total_revenue': locale.format("%d", ao_total_revenue, grouping=True),
    'ao_total_margin': locale.format("%d", ao_total_margin, grouping=True),
    'ao_top_legend': ao_top_legend,
    'date': current_date,
    'total_revenue': total_revenue,
    'total_margin': total_margin
})

m.send_mail(
    'Pmp Status for {0} - ${1}'.format(current_date, total_revenue),
    'cbeecher@rhythmone.com'
    # cc=['pmpops@rhythmone.com', 'krayes@rhythmone.com', 'jmurphy@rhythmone.com', 'llam@rhythmone.com']
)

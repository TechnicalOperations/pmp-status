import os
import re
import pandas
import MySQLdb
import numpy as np
from util import sql
from io import StringIO
from report_mailer import mailer
from datetime import datetime, timedelta
pandas.set_option('display.width', 5000)


BASE_AUTH_DIR = '/var/auth/'
# BASE_AUTH_DIR = 'D:\\Work\\auth\\'
con = MySQLdb.connect(read_default_file=BASE_AUTH_DIR + '.my.conf', db='dashboard')
m = mailer.Mailer('/Users/cbeecher/.auth/.em_json', 'categories.html')

date = datetime.now()
date = date - timedelta(hours=date.hour, days=1)
q = """
	SELECT *
	FROM index_pmpperformance
	WHERE date >= STR_TO_DATE('{start}', '%Y-%m-%d')
""".format(start=date.strftime('%Y-%m-%d'))

data = pandas.read_sql_query(q, con)
con.close()
data = data[['date', 'deal_id', 'dsp_id', 'revenue', 'margin']]
data = data.groupby(['date', 'deal_id', 'dsp_id']).sum().reset_index()
data = data[~data['deal_id'].isnull() & (data['deal_id'] != '')]

radium_ids = ['1227', '1226', '1225', '1224', '1224', '1224']
q = """
SELECT A.external_deal_id AS deal_id,
    A.description,
    CASE WHEN A.type = 1 THEN 'Legacy Direct'
        WHEN A.type = 2 THEN 'Always On'
        WHEN A.type = 3 THEN 'Managed'
        WHEN A.type = 4 THEN 'Self Serve'
        WHEN A.type = 5 THEN 'Test'
    ELSE 'none_selected' END AS deal_type,
    IFF(B.dsp_id IN ({0}), 1, 0) AS is_radium
FROM ods.rx.deal AS A
LEFT JOIN ods.rx.deal_dsp AS B ON A.deal_id=B.deal_id
GROUP BY A.external_deal_id, A.description, deal_type, is_radium
""".format(", ".join(radium_ids))
try:
    details = pandas.read_excel('details.xlsx')
    print("Read From File")
except FileNotFoundError:
    con = sql.connect()
    details = pandas.read_sql_query(q, con)
    sql.close()
    details.to_excel('details.xlsx', index=False)
details = details.rename(columns={i: i.lower() for i in details.columns})

data = data.merge(details, on='deal_id', how='left')
data['is_radium'] = data['dsp_id'].apply(lambda x: 'RadiumOne DSP' if 'radium' in x else 'All Other DSPs')
del data['dsp_id']
del data['deal_id']
del data['description']
del data['date']
data['Margin %'] = 0
data = data.rename(columns={'margin': 'Margin', 'revenue': 'Revenue'})
data = pandas.pivot_table(data, index=['deal_type'], columns=['is_radium'], aggfunc=np.sum, fill_value=0)
data['Margin %'] = data['Margin'] * 100.0 / data['Revenue']
data['Margin %'] = data['Margin %'].fillna(0)

data['Revenue'] = data['Revenue'].applymap(lambda x: '${0}'.format(m.format_float(x, round=2)))
data['Margin'] = data['Margin'].applymap(lambda x: '${0}'.format(m.format_float(x, round=2)))
data['Margin %'] = data['Margin %'].applymap(lambda x: '{0}%'.format(m.format_float(x, round=2)))

data.columns = data.columns.swaplevel(0, 1)
order = [['RadiumOne DSP', 'All Other DSPs'], ['Revenue', 'Margin', 'Margin %']]
cols = list(data.columns)
print(cols)
cols.sort(key=lambda x: (order[0].index(x[0]), order[1].index(x[1]),))
data = data[cols]

data.columns.names = [None, None]
data.index.name = date.strftime('%Y-%m-%d')

all_other = StringIO()
radium = StringIO()
data['All Other DSPs'].reset_index().to_html(all_other, index=False)
all_other.seek(0)
all_other = all_other.read()
data['RadiumOne DSP'].reset_index().to_html(radium, index=False)
radium.seek(0)
radium = radium.read()

print(data[:10])
os.remove('details.xlsx')
r = re.compile('<tr.*?</tr>', re.DOTALL)
all_other = r.findall(all_other)
radium = r.findall(radium)

output = []
for index in range(len(all_other)):
    arow = all_other[index].split('\n')
    arow = [i.strip() for i in arow if '<td' in i or '<th' in i]
    rrow = radium[index].split('\n')
    rrow = [i.strip() for i in rrow if '<td' in i or '<th' in i]
    tmp = arow + rrow
    tmp = '<tr>\n\t{0}\n</tr>'.format('\n\t'.join(tmp))
    output.append(tmp)

print('\n'.join(output))

m.set_formatter(
    formatter={
        'table': '\n'.join(output)
    }
)
m.send_mail(
    'PMP Category Performance',
    'cbeecher@rhythmone.com',
    cc=['mvillaseca@rhythmone.com']
)
